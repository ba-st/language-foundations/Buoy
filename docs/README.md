# Buoy Documentation

Buoy aims to complement [Pharo](https://www.pharo.org) adding useful extensions.

To learn about the project, [install it](how-to/how-to-load-in-pharo.md) and
[follow the assertions tutorial](tutorial/Assertions.md), or expand your
understanding over specific topics:

- **Collections**: Additional abstractions for Collections.
  See the [related documentation.](reference/Collections.md)
- **Comparison**: Support to compare objects both for equality and identity.
  They are typically used to implement the `=` and `hash` methods. See the
[related documentation.](reference/Comparison.md)
- **Math**: Basic arithmetic abstractions like Percentages. See the
[related documentation.](reference/Math.md)
- **Bindings and Optionals**: Support to express optional values and
  required values, that can be unknown at the beginning of an execution.
  See the [related documentation.](reference/BindingsAndOptionals.md)
- **Exception Handling**: Extensions to the [exception handling mechanics](reference/ExceptionHandling.md).
- **Metaprogramming**: Some abstractions like [namespaces](reference/Namespaces.md)
and [interfaces](reference/Interfaces.md).
- **SUnit**: [Extensions to the SUnit framework](reference/SUnit.md).

---

To use the project as a dependency of your project, take a look at:

- [How to use Buoy as a dependency](how-to/how-to-use-as-dependency-in-pharo.md)
- [Baseline groups reference](reference/Baseline-groups.md)
